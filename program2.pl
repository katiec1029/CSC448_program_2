%list of people
person(mary).
person(jim).
person(john).

%list of jobs
job(gardener).
job(vetAssistant).
job(dishWasher).
job(nurse).
job(highSchoolMathTeacher).
job(biologyTutor).

%length of variables are 2 &
%Solution is person and corresponding variables
length(MaryJobs,2), length(JimJobs,2), length(JohnJobs,2),
Solution = [mary-MaryJobs,jim-JimJobs,john-JohnJobs],

%query to find the jobs AllJobs is a list containing variables of the jobs
findall(Jobs,job(Job),AllJobs),
AllJobs = [Gardener,VetAssistant,DishWasher,Nurse,MathTeacher,BioTutor],

flatten([MaryJobs,JimJobs,JohnJobs],Jobs),
permutation(Jobs,AllJobs),

% 6 & 9; Mary is not the Nurse, VetAssistant, or BioTutor
\+ member(Nurse,MaryJobs),
\+ member(VetAssistant,MaryJobs),
\+ member(BioTutor, MaryJobs),

% 7 & 8 & 3 ; Jim is not the MathTeacher or Gardener or Nurse (dropped out)
\+ member(MathTeacher,JimJobs),
\+ member(Gardener, JimJobs),
\+ member(Nurse, JimJobs),

%Mary is the Gardener because Jim does not know the
%Gardener, therefore he cannot have gone out with the Gardener.
\+ member(Gardener, JohnJobs),

%Jim must not KNOW Mary because she is the Gardener
%John and Mary must have been married
%Conclusion: Jim is not the Bio Tutor
\+ member(BioTutor, JimJobs),

%logically, since Jim likes animals, it would make sense if he
%were the VetAssistant and since this is true, John is the nurse
\+ member(VetAssistant, JohnJobs),
\+ member(Nurse, JimJobs),
%logically since jim dropped out of college, it would make sense
%if he were to be the dishwasher
\+ member(DishWasher, MaryJobs),
\+ member(DishWasher, JohnJobs).
%Automatically this is should conclude that Mary is the MathTeacher

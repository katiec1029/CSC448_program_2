Note: These Comments were in the file initially
%There are three people: John, Jim, and Mary and each has two jobs.
%The jobs are gardener, veterinarian's  assistant, dishwasher, nurse,
%high school math teacher, and biology tutor.

%You are given the following information:
%1) The nurse went out with the veterinarian's assistant last night.
%2) Mary is friends with the biology tutor but she stayed home last night.
%3) Jim likes animals but he failed math and had to drop out of college.
%4) Jim does not know the gardener.
%5) Mary and the biology tutor used to be married.

%My Conclusions from Facts:
%6) Mary is not the biologyTutor because she did not go out with the
% biologyTutor and she was married to the biology tutor (2&5)
%7) Jim is not the highSchoolMathTeacher because he failed math (3)
%8) Jim is not the gardener because he does not know the gardener (4)
%9) Mary is not the nurse/vetAssistant because she did not go out (1&2)
%10) The biologyTutor went out last night (1&2)

%Who holds which jobs? Include a report explaining your strategy.

%Exepcted Answers:
%Mary: gardener, highSchoolMathTeacher
%Jim: vetAssistant, dishWasher
%John: nurse, biologyTutor

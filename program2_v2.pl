%list of people
person(mary).
person(jim).
person(john).

%list of jobs
job(gardener).
job(vetAssistant).
job(dishWasher).
job(nurse).
job(highSchoolMathTeacher).
job(biologyTutor).

went_Out(nurse,vetAssistant).
friends(mary,biologyTutor).
dislikesMath(jim).
droppedOut(jim).
likesAnimals(jim).
unknown(jim,gardener).
divorced(mary,biologyTutor).

maryJobs(mary, job(Job), job(Job)),
jimJobs(jim, job(Job), job(Job)),
johnJobs(john, job(Job), job(Job)).

% 6 & 9; Mary is not the Nurse, VetAssistant, or BioTutor
went_Out(nurse, vetAssistant),
friends(mary, biologyTutor),
divorced(mary, biologyTutor) :-
    \+ maryJobs(mary, nurse, vetAssistant),
    \+ maryJobs(mary, biologyTutor, nurse),
    \+ maryJobs(mary, vetAssistant, biologyTutor).

% 7 & 8 & 3 ; Jim is not the MathTeacher or Gardener or Nurse (dropped out)
dislikesMath(jim),
droppedOut(jim),
unknown(jim, gardener),
likesAnimals(jim):-
    \+ jimJobs(jim,highSchoolMathTeacher,nurse),
    \+ jimJobs(jim,gardener,highSchoolMathTeacher),
    \+ jimJobs(jim,nurse,gardener),
    jimJobs(jim,vetAssistant,X),
    jimJobs(jim,X,vetAssistant).

%Mary is the Gardener because Jim does not know the
%Gardener, therefore he cannot have gone out with the Gardener.
unknown(jim, gardener),
went_Out(nurse, vetAssistant):-
    maryJobs(gardener, X),
    maryJobs(X, gardener).

%Jim must not KNOW Mary because she is the Gardener
%John and Mary must have been married
%Conclusion: Jim is not the Bio Tutor
unknown(jim,gardener),
person(mary):-
    divorced(mary,biologyTutor),
    person(john),
    \+ jimJobs(jim, X, biologyTutor),
    \+ jimJobs(jim, biologyTutor, X).

%logically since jim dropped out of college, it would make sense
%if he were to be the dishwasher
droppedOut(jim):-
     jimJobs(jim, X, dishWasher),
     jimJobs(jim, dishWasher, X).

%Automatically this is should conclude that Mary is the MathTeacher


%query to find the jobs AllJobs is a list containing variables of the jobs
findAllJobs(maryJobs,Job1, Job2),
findAllJobs(jimJobs, Job1, Job2),
findAllJobs(johnJobs, Job1, Job2).
